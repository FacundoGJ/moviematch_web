import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Inicio from './views/Inicio.vue';
import Compra from './views/Compra.vue';
import CompraStep2 from './views/CompraStep2.vue';
import CompraStep3 from './views/CompraStep3.vue';
import CompraStep4 from './views/CompraStep4.vue';

Vue.use(Router);

const scrollBehavior = to => {
  if (to.hash !== '#start-here') {
    return {
      selector: to.hash,
      offset: { x: 0, y: -100 },
    };
  } else {
    return {
      x: 0,
      y: 0,
    };
  }
};

export default new Router({
  // mode: 'history', this brock navigation in folder
  base: '',
  routes: [
    {
      path: '/home',
      name: 'home',
      hash: '#app',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: Home,
    },
    {
      path: '/',
      name: 'inicio',
      component: Inicio,
      meta: {
        title:
          'Movie Match | Tu salida al cine con amigos más fácil y sencilla',
      },
    },
    {
      path: '/compraStep1-:id',
      name: 'compra-1',
      component: Compra,
      meta: {
        title: 'Movie Match | Paso 1 de compra',
      },
    },
    {
      path: '/compraStep2-:id',
      name: 'compra-2',
      component: CompraStep2,
      meta: {
        title: 'Movie Match | Paso 2 de compra',
      },
    },
    {
      path: '/compraStep3-:id',
      name: 'compra-3',
      component: CompraStep3,
      meta: {
        title: 'Movie Match | Paso 3 de compra',
      },
    },
    {
      path: '/compraStep4-:id',
      name: 'compra-4',
      component: CompraStep4,
      meta: {
        title: 'Movie Match | Paso 4 de compra',
      },
    },
  ],
  scrollBehavior,
});
