export default [
  {
    id: 0,
    title: 'El mayor regalo',
    sinopsis:
      'Alguien va a matar a alguien. Ojalá fuera una película. ¿Nos rendimos... o buscamos otro final feliz? Existe un arma capaz de poner fin a cualquier conflicto. Ya la han usado en Francia, España, Irlanda, México, Colombia, Ruanda... y funciona, siempre. Su poder constructivo es ilimitado. Y, además, es gratis.',
    imageName: '0.jpg',
    alt: 'El mayor regalo',
    duration: '105 min',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Documental',
      director: 'Juan Manuel Cotelo',
      actores:
        'Santiago Rodriguez, Juan Manuel Cotelo, Carlos Aguillo, Ines Sahara, Carlos Chamarro, Claudia Duran, Paco Estelles',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/-dT14yGj5bI',
    stars: 4,
  },
  {
    id: 1,
    title: 'Infiltrado del KKKlan',
    sinopsis:
      'En 1979, Ron se convierte en el primer policía negro de Colorado Springs, pero sus superiores no le reciben de buen grado. Ron y su compañero Flip, judío, afrontan una misión casi suicida: infiltrarse en el Ku Klux Klan local y desmantelarlo.',
    imageName: '1.jpg',
    alt: 'Infiltrado del KKKlan',
    duration: '2 hr 15 min',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Drama/Cine policíaco',
      director: 'Spike Lee',
      actores:
        'Rami Malek, Ben Hardy, Gwilym Lee, Joseph Mazzello, Allen Leech, Lucy Boynton, Mike Myers, Aaron McCusker, Aidan Gillen, Tom Hollander',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/8EBTEDc6Gis',
    stars: 2,
  },
  {
    id: 2,
    title: 'Los crímenes de Grindelwald',
    sinopsis:
      'Al final de la primera película, Gellert Grindelwald, el poderoso mago oscuro, cayó en manos de MACUSA, gracias a la ayuda de Newt Scamander. Pero, fiel a su amenaza, Grindelwald huyó y se dispuso a reunir seguidores. Casi nadie sospecha cuál es su verdadero plan: agrupar magos de sangre pura para gobernar a todos los seres no mágicos. ',
    imageName: '2.jpg',
    alt: 'Los crímenes de Grindelwald',
    duration: '2:32hs',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Aventuras',
      director: 'Davis Yates',
      actores: 'Johnny Depp, Jude Law, Eddie Redmayne',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/XAMMhxpeKuU',
    stars: 5,
  },
  {
    id: 3,
    title: 'Bohemian Rhapsody',
    sinopsis:
      'El cantante Freddie Mercury, el guitarrista Brian May, el baterista Roger Taylor y el bajista John Deacon forman la banda británica de rock Queen en los años 70. Es en 1975 cuando su sencillo Bohemian Rhapsody los coloca en un primer plano de la escena musical internacional.',
    imageName: '3.jpg',
    alt: 'Bohemian Rhapsody',
    duration: '2:32hs',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Biografia',
      director: 'Bryan Singer',
      actores: 'Rami Malek, Joseph Mazzello, Mike Myers',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/01Sn153r9RY',
    stars: 5,
  },
  {
    id: 4,
    title: 'El Grinch',
    sinopsis:
      'El Grinch cuenta la historia de un cínico gruñón que se embarca en la misión de robar la Navidad, solo para que sus sentimientos cambien gracias al generoso espíritu festivo de una pequeña niña. Graciosa, conmovedora y visualmente asombrosa, es una historia universal acerca del espíritu navideño y el indomable poder del optimismo.',
    imageName: '4.jpg',
    alt: 'El Grinch',
    duration: '2:32hs',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Animación',
      director: 'Yarrow Cheney, Scott Mosier',
      actores: 'Benedict Cumberbatch',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/6vrILSnlWMM',
    stars: 5,
  },
  {
    id: 5,
    title: 'Pablo Escobar: la traición',
    sinopsis:
      'La historia del líder del cártel de Medellín, Pablo Escobar, narrada desde la perspectiva de la periodista colombiana Virginia Vallejo, quien mantuvo una relación sentimental con el narcotraficante.',
    imageName: '5.jpg',
    alt: 'Pablo Escobar: la traición',
    duration: '2:32hs',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Biografía',
      director: 'Fernando León de Aranoa',
      actores:
        'Josh Hutcherson, Benicio Del Toro, Brady Corbet, Claudia Traisac, Carlos Bardem, Ana Girardot, Laura Londoño',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/htqW4KFvWwM',
    stars: 3,
  },

  {
    id: 6,
    title: 'Halloween',
    sinopsis: 'sarasa',
    imageName: '6.jpg',
    alt: 'Halloween',
    duration: '2:32hs',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Terror',
      director: 'Director',
      actores:
        'Rami Malek, Ben Hardy, Gwilym Lee, Joseph Mazzello, Allen Leech, Lucy Boynton, Mike Myers, Aaron McCusker, Aidan Gillen, Tom Hollander',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/_Esb14TaGrk',
    stars: 5,
  },
  {
    id: 7,
    title: 'Aquaman',
    sinopsis:
      'Warner Bros. Pictures y el director James Wan nos traen una aventura llena de acción que abarca el vasto y sorprendente mundo submarino de los siete mares, "Aquaman", protagonizada por Jason Momoa en el papel principal. La película revela la historia de origen de Arthur Curry, mitad humano y mitad atlante, y lo lleva al viaje de su vida, uno que no solo lo forzará a enfrentar quién es en realidad, sino a descubrir si es digno de quién es él. nació para ser ... un rey.',
    imageName: '7.jpg',
    alt: 'Aquaman',
    duration: '2:32hs',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Acción',
      director: 'James Wan',
      actores: 'Acción',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/WDkg3h8PCVU',
    stars: 5,
  },
  {
    id: 8,
    title: 'Fátima',
    sinopsis: 'sarasa',
    imageName: '8.jpg',
    alt: 'Fátima',
    duration: '2:32hs',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Drama, Música',
      director: 'Director',
      actores:
        'Rami Malek, Ben Hardy, Gwilym Lee, Joseph Mazzello, Allen Leech, Lucy Boynton, Mike Myers, Aaron McCusker, Aidan Gillen, Tom Hollander',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/MEfQudthSxE',
    stars: 5,
  },
  {
    id: 9,
    title: 'Christopher Robin',
    sinopsis: 'sarasa',
    imageName: '9.jpg',
    alt: 'Christopher Robin',
    duration: '2:32hs',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Drama, Música',
      director: 'Director',
      actores:
        'Rami Malek, Ben Hardy, Gwilym Lee, Joseph Mazzello, Allen Leech, Lucy Boynton, Mike Myers, Aaron McCusker, Aidan Gillen, Tom Hollander',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/EpJrFtcP0Ok',
    stars: 5,
  },
  {
    id: 0,
    title: 'El mayor regalo',
    sinopsis:
      'Alguien va a matar a alguien. Ojalá fuera una película. ¿Nos rendimos... o buscamos otro final feliz? Existe un arma capaz de poner fin a cualquier conflicto. Ya la han usado en Francia, España, Irlanda, México, Colombia, Ruanda... y funciona, siempre. Su poder constructivo es ilimitado. Y, además, es gratis. Pero hay que ser muy valiente para usarla. Los griegos le llaman "hyper-don": EL MAYOR REGALO Ficción y documental, comedia y Drama, como la vida misma!',
    imageName: '0.jpg',
    alt: 'El mayor regalo',
    duration: '105 min',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Documental',
      director: 'Juan Manuel Cotelo',
      actores:
        'Santiago Rodriguez, Juan Manuel Cotelo, Carlos Aguillo, Ines Sahara, Carlos Chamarro, Claudia Duran, Paco Estelles',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/-dT14yGj5bI',
    stars: 4,
  },
  {
    id: 1,
    title: 'Infiltrado del KKKlan',
    sinopsis:
      'En 1979, Ron se convierte en el primer policía negro de Colorado Springs, pero sus superiores no le reciben de buen grado. Ron y su compañero Flip, judío, afrontan una misión casi suicida: infiltrarse en el Ku Klux Klan local y desmantelarlo.',
    imageName: '1.jpg',
    alt: 'Infiltrado del KKKlan',
    duration: '2 hr 15 min',
    fichaTecnica: {
      origen: 'EEUU',
      genero: 'Drama/Cine policíaco',
      director: 'Spike Lee',
      actores:
        'Rami Malek, Ben Hardy, Gwilym Lee, Joseph Mazzello, Allen Leech, Lucy Boynton, Mike Myers, Aaron McCusker, Aidan Gillen, Tom Hollander',
      distribuidora: 'Fox',
    },
    linkYoutubeTrailer: 'https://www.youtube.com/embed/8EBTEDc6Gis',
    stars: 2,
  },
];
