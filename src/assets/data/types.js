export default {
  idiomas: [
    {
      value: 'Subtitulada',
      label: 'Subtitulada',
    },
    {
      value: 'Castellano',
      label: 'Castellano',
    },
  ],
  salas: [
    {
      value: '2D',
    },
    {
      value: '3D',
    },
  ],
  franjasHoraria: [
    {
      value: 'tarde',
      label: 'Tarde',
      times: [
        {
          value: '1',
          label: '15:30 PM',
        },
        {
          value: '2',
          label: '16:10 PM',
        },
        {
          value: '3',
          label: '17:20 PM',
        },
        {
          value: '4',
          label: '18:50 PM',
        },
      ],
    },
    {
      value: 'noche',
      label: 'Noche',
      times: [
        {
          value: '5',
          label: '09:30 PM',
        },
        {
          value: '6',
          label: '10:20 PM',
        },
        {
          value: '7',
          label: '10:50 PM',
        },
        {
          value: '8',
          label: '11:30 PM',
        },
      ],
    },
    {
      value: 'trasnoche',
      label: 'Trasnoche',
      times: [
        {
          value: '9',
          label: '00:20 AM',
        },
        {
          value: '10',
          label: '00:50 AM',
        },
        {
          value: '11',
          label: '01:15 AM',
        },
        {
          value: '12',
          label: '01:30 AM',
        },
      ],
    },
  ],
};
