require('lodash');
import Vue from 'vue';
import Vuex from 'vuex';
import peliculas from './assets/data/peliculas';
import cines from './assets/data/cines';
import types from './assets/data/types';
import butacasDisabled from './assets/data/butacasDisabled';

Vue.use(Vuex);
const selectedDefault = {
  date: '',
  butacasSelected: [],
  butacasAPagar: 1,
  cineSelected: '',
  salaSelected: '',
  idiomaSelected: '',
  franjaSelected: '',
  timeSelected: {},
  peliculaId: 0,
};
const CloneSelectedDefault = _.cloneDeep(selectedDefault);
export default new Vuex.Store({
  state: {
    peliculas,
    cines,
    types,
    butacasDisabled,
    ariaLive: { msg: '', emit: '' },
    userSelected: {
      ...CloneSelectedDefault,
    },
  },
  getters: {
    peliculas(state) {
      return state.peliculas;
    },
    cines(state) {
      return state.cines;
    },
    types(state) {
      return state.types;
    },
    butacasDisabled(state) {
      return state.butacasDisabled;
    },
    userSelected(state) {
      return state.userSelected;
    },
    peliculaId(state) {
      return state.userSelected.peliculaId;
    },
    pelicula(state) {
      const peliculas = _.cloneDeep(state.peliculas);
      return peliculas.find(
        pelicula => pelicula.id === _.parseInt(state.userSelected.peliculaId),
      );
    },
    hora(state) {
      let label = '';
      _.forEach(state.types.franjasHoraria, franja => {
        _.forEach(franja.times, time => {
          if (time.value === state.userSelected.timeSelected) {
            label = time.label;
          }
        });
      });
      return label;
    },
    idioma(state) {
      const idioma = state.types.idiomas.find(
        idioma => idioma.value === state.userSelected.idiomaSelected,
      );
      return idioma ? idioma.label : '';
    },
    precio(state) {
      return state.userSelected.salaSelected === '3D' ? 230 : 160;
    },
    ariaLive(state) {
      return state.ariaLive;
    },
  },
  mutations: {
    resetData(state) {
      localStorage.userSelected = '{}';
      Vue.set(state, 'userSelected', CloneSelectedDefault);
    },
    ariaLive(state, { msg }) {
      Vue.set(state.ariaLive, 'msg', msg);
      Vue.set(state.ariaLive, 'emit', Math.random());
    },
    userSelected(state, { property, value }) {
      Vue.set(state.userSelected, property, value);
    },
    peliculaId(state, { id }) {
      Vue.set(state.userSelected, 'peliculaId', id);
    },
  },
  actions: {},
});
