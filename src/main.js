require('lodash');
window.$ = window.jQuery = require('jquery');
import 'jquery-ui/ui/widgets/datepicker';
import 'jquery-ui/ui/i18n/datepicker-es';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Vuetify from 'vuetify';
import './assets/scss/main.scss';
import VeeValidate from 'vee-validate';
const VueValidationEs = require('vee-validate/dist/locale/es');

const config = {
  locale: 'es',
  dictionary: {
    es: VueValidationEs,
  },
};

Vue.use(VeeValidate, config);
Vue.use(Vuetify, {
  theme: {
    primary: '#3B66B8',
    secondary: '#39CCCC',
    accent: '#FF824B',
    error: '#D62B00',
    info: '#3B66B8',
    success: '#008014',
    warning: '#FFC107',
  },
});
Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});
router.afterEach(to => {
  if (to.hash) {
    setTimeout(() => {
      $(to.hash)[0] ? $(to.hash)[0].focus() : undefined;
    }, 100);
  }
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
